import React from 'react'
import { graphql, StaticQuery } from 'gatsby'
import styled from 'styled-components'
import "./layout.css"

import BackgroundImage from 'gatsby-background-image'

const BackgroundSection = ({ className, children }) => (
  <StaticQuery
    query={graphql`
      query {
        desktop: file(relativePath: { eq: "alex-loup-unsplash-low.jpg" }) {
          childImageSharp {
            fluid(quality: 90, maxWidth: 1920) {
              ...GatsbyImageSharpFluid_withWebp
            }
          }
        }
      }
    `}
    render={data => {
      // Set ImageData.
      const imageData = data.desktop.childImageSharp.fluid
      return (
        <BackgroundImage
          Tag="section"
          className={className}
          fluid={imageData}
          backgroundColor={`#040e18`}
        >
        <StyledInnerWrapper>
          <h1 style={{color:`#666699`, 'margin-left': `50px`}}> LifeX </h1>
          <h2 style={{color:`#666699`, 'margin-left': `50px`}}> Reinventing life insurance from scratch </h2>
          <main>{children}</main>
        </StyledInnerWrapper>
        </BackgroundImage>
      )
    }}
  />
)

const StyledInnerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding-top: 150px;
  margin-left: 30px;
  position: relative;
`

const StyledBackgroundSection = styled(BackgroundSection)`
  width: 100%;
  background-position: bottom center;
  background-repeat: no-repeat;
  background-size: cover;
  height: 100vh;
  position: relative;
`

export default StyledBackgroundSection
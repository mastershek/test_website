import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import StyledBackgroundSection from "../components/styled-background"
import SEO from "../components/seo"
import { Button, TextField } from "@material-ui/core"

export default class IndexPage extends React.Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
  }
  handleInputChange = event => {
    const target = event.target
    const value = target.value
    const name = target.name
    this.setState({
      [name]: value,
    })
  }
  handleSubmit = event => {
    alert(`Welcome ${this.state.firstName} ${this.state.lastName}!`)
    event.preventDefault()
  }
  render() {
    return (
        <StyledBackgroundSection>
          <div className="container" style={{'margin-left': `50px`}}>
              <form action="https://formspree.io/mqkynelq" method="POST">
                 <div>
                  <TextField
                    label="First Name"
                    type="text"
                    name="firstName"
                    variant="outlined"
                    color="#666699"
                    margin="dense"
                    value={this.state.firstName}
                    onChange={this.handleInputChange}
                  />
                </div>
                <br/>
                <div>
                  <TextField
                    label="Last Name"
                    type="text"
                    name="lastName"
                    variant="outlined"
                    value={this.state.lastName}
                    onChange={this.handleInputChange}
                  />
                </div>
                <br/>
                <div>
                  <TextField
                    label="Email"
                    type="text"
                    name="email"
                    variant="outlined"
                    value={this.state.email}
                    onChange={this.handleInputChange}
                  />
                </div>
                <br/>
                <div>
                  <Button variant="contained" color="primary" onClick={(event) => {this.handleSubmit(event)}}>Submit</Button>
                </div>
              </form>
           </div>
        </StyledBackgroundSection>
    )
  }
}


